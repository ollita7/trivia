interface Window {
    externalObject: {
        confetti: () => void,
        jquery: () => void,
    }
}