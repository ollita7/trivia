import { useQuery, useMutation } from 'react-query';
import { TriviaRepository } from "../repositories/trivia";
import {  QUERIES_KEYS } from '../queryKeys';

export interface IAnswer {
  triviaId: number;
  answer: string;
  runId: string;
  questionId: string;
  playerName: string;
  group: string;
  responseTime: string;
}

const useGetTrivia = (id: number): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery([`${QUERIES_KEYS.GET_TRIVIA}`, id], TriviaRepository.get, {
    enabled: !!id,
    select: (response) => {
      return response
    },
  });
}

const useGetTotalScores = (): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery([`${QUERIES_KEYS.GET_TOTAL_SCORES}`], TriviaRepository.getTotalScores, {
    enabled: true,
    select: (response) => {
      return response
    },
  });
}

const useGetAffiliates = (): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery([`${QUERIES_KEYS.GET_AFFILIATES}`], TriviaRepository.getAffiliates, {
    enabled: true,
    select: (response) => {
      return response
    },
  });
}



const useGetTriviaRun = (id: string): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery([`${QUERIES_KEYS.GET_TRIVIA}`, id], TriviaRepository.getRun, {
    enabled: !!id,
    select: (response) => {
      return response
    },
  });
}

const useGetTriviaResults = (id: string): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery([`${QUERIES_KEYS.GET_TRIVIA}`, id], TriviaRepository.getResults, {
    enabled: !!id,
    select: (response) => {
      return response
    },
  });
}

const useGetTrivias = (): {
  isLoading: boolean;
  isSuccess: boolean;
  isError: boolean;
  data: any;
  refetch: () => void;
} => {
  return useQuery([`${QUERIES_KEYS.GET_TRIVIAS}`], TriviaRepository.list, {
    enabled: true,
    select: (response) => {
      return response
    },
  });
}

const useStart = () => {
  const mutation = useMutation((id: number) => {
    return TriviaRepository.start(id);
  });
  return mutation;
};

const useRun = () => {
  const mutation = useMutation((id: number) => {
    return TriviaRepository.run(id);
  });
  return mutation;
};

const useAnswer = () => {
  const mutation = useMutation((data: IAnswer) => {
    return TriviaRepository.answer(data)
  });
  return mutation;
};

const useEnd = () => {
  const mutation = useMutation(() => {
    return TriviaRepository.end();
  });
  return mutation;
};

const useCleanup = () => {
  const mutation = useMutation(() => {
    return TriviaRepository.cleanup();
  });
  return mutation;
};


export { useGetTrivia, useGetTrivias, useStart, useEnd, useGetTriviaRun, useAnswer, useGetTriviaResults, useGetTotalScores, useGetAffiliates, useRun, useCleanup }