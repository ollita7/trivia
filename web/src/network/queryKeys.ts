export const enum QUERIES_KEYS {
  USER_GET_PROFILE = 'user/get_profile',
  GET_TRIVIA = 'get_trivia',
  GET_TRIVIAS = 'get_trivias',
  GET_RUN = 'get_run',
  GET_AFFILIATES = 'get_affiliates',
  GET_TOTAL_SCORES = 'get_total_scores'
}