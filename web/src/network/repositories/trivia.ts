import { BaseClient } from '../appClient';
import { IAnswer } from '../services/trivia.service';

const ENDPOINTS = {
  getTriviaUrl: (id: number) => `trivia/${id}`,
  getTriviaRunUrl: (id: string) => `trivia/run/${id}`,
  getTriviaResultsUrl: (id: string) => `trivia/${id}/results`,
  getTriviasUrl: () => `trivia`,
  answerUrl: () => `trivia/answer`,
  postStartUrl: (id:number) => `trivia/${id}/start`,
  postRunUrl: (id:number) => `trivia/${id}/run`,
  postEndUrl: () => `trivia/end`,
  getTotalScoresUrl: () => `trivia/total_scores`,
  getAffiliatesUrl: () => `affiliate`,
  cleanupUrl: () => `trivia/cleanup`,
} 

const get = (parameters: any): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getTriviaUrl(parameters.queryKey[1]));
};

const getAffiliates = (): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getAffiliatesUrl());
};

const getTotalScores = (): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getTotalScoresUrl());
};

const getRun = (parameters: any): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getTriviaRunUrl(parameters.queryKey[1]));
};

const getResults = (parameters: any): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getTriviaResultsUrl(parameters.queryKey[1]));
};

const list = (): Promise<any> => {
  return BaseClient.get(ENDPOINTS.getTriviasUrl());
};

const start = (id: number): Promise<any> => {
  return BaseClient.post(ENDPOINTS.postStartUrl(id));
};

const run = (id: number): Promise<any> => {
  return BaseClient.post(ENDPOINTS.postRunUrl(id));
};

const end = (): Promise<any> => {
  return BaseClient.post(ENDPOINTS.postEndUrl());
};

const answer = (data: IAnswer): Promise<any> => {
  return BaseClient.post(ENDPOINTS.answerUrl(), data);
};

const cleanup = (): Promise<any> => {
  return BaseClient.delete(ENDPOINTS.cleanupUrl());
};

export const TriviaRepository = {
  get, list, start, end, getRun, answer, getResults, getTotalScores, getAffiliates, run, cleanup
}