import React, { useEffect } from 'react';
import gsap from 'gsap';
import { Outlet } from 'react-router-dom';
import { connect } from "react-redux";
import { IStoreDispatchProps } from '../store/storeComponent';
import { getPlayer } from '../store/selectors';
import { RootState } from "../store/store";
import { IPlayerState } from '../store/reducers/player';
import background from '../assets/components/background.mp4';
import ball_top from '../assets/img/ball-top.png';
import ball_bottom from '../assets/img/ball-bottom.png';
import logo from '../assets/img/logo.svg';

interface ILayoutProps extends IStoreDispatchProps {
  player: IPlayerState;
}

const Layout = ({player, ...props }: ILayoutProps): React.ReactElement => {

  // Anims
  useEffect(() => {
    gsap.fromTo("#login .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2, delay: 0.25 })
  }, [])
 
  return (
    <div className='common'>
      <div className="background">
        <video src={background} autoPlay loop muted playsInline></video>
        <img src={ball_top} className="top-bg"/>
        <img src={ball_bottom} className="bottom-bg"/>
      </div>
      <div id="login" className="container login col-10 col-lg-3">
        <div className="col-12 logo-login mb-5 stag">
            <img src={logo} alt="ONCONNECT"/>
        </div>
        <Outlet />
      </div>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(Layout);