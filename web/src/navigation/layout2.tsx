import React from 'react';
import { Outlet } from 'react-router-dom';
import { connect } from "react-redux";
import { IStoreDispatchProps } from '../store/storeComponent';
import { getPlayer } from '../store/selectors';
import { RootState } from "../store/store";
import { IPlayerState } from '../store/reducers/player';
import background from '../assets/components/background.mp4';
import ball_top from '../assets/img/ball-top.png';
import ball_bottom from '../assets/img/ball-bottom.png';
import logo from '../assets/img/logo.svg';
import break_img from '../assets/components/break.webp';

interface ILayoutProps extends IStoreDispatchProps {
  player: IPlayerState;
}

const Layout2 = ({player, ...props }: ILayoutProps): React.ReactElement => {
 
  return (
    <div className='common'>
      <div className="background">
        <video src={background} autoPlay loop muted playsInline></video>
        <img src={ball_top} className="top-bg"/>
        <img src={ball_bottom} className="bottom-bg"/>
      </div>

      { window.location.pathname === '/admin/leaderboard' ? 
        <div className="container screen col-10 col-lg-8">
          <header>
            <div className="col-12 logo-header">
                <img src={logo} alt="ONCONNECT"/>
            </div>
            <div className="break">
                <img src={break_img} alt="ONCONNECT"/>
            </div>
          </header>
          <Outlet />
        </div>
      :
      <div className="container screen col-10 col-lg-4">
          <header>
            <div className="col-12 logo-header">
                <img src={logo} alt="ONCONNECT"/>
            </div>
            <div className="break">
                <img src={break_img} alt="ONCONNECT"/>
            </div>
          </header>
          <Outlet />
        </div>
      
      }


    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(Layout2);