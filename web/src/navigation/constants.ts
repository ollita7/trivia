enum ROUTES {
  LOGIN = '/login',
  ADMIN_START = '/admin/start',
  ADMIN_GAME = '/admin/game',
  ADMIN_LEADERBOARD = '/admin/leaderboard',
  LEADERBOARD = '/leaderboard',
  GAME = '/game',
  ADMIN_THANKS = '/admin/thanks',
  THANKS = '/thanks',
  JOIN = '/join',
  RECONNECTING = '/reconnecting',
  THANKS_ENDED = '/ended',
  WILDCARD = '*',
}

export {ROUTES}