import React, { Fragment, useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import { connect } from "react-redux";
import { Navigate } from 'react-router-dom';
import { ROUTES } from './constants';
//import { Layout } from './layout';
import { IStoreDispatchProps } from '../store/storeComponent';
import { useGetProfile } from '../network/services/user.service';
import { getProfile } from '../store/selectors';
import { RootState } from "../store/store";
import { IProfileState, setProfile } from '../store/reducers/profile';
import background from '../assets/img/background.webp';
import ball_top from '../assets/img/ball-top.png';
import ball_bottom from '../assets/img/ball-bottom.png';
import logo from '../assets/img/logo.svg';

interface IProps extends IStoreDispatchProps {
  profile: IProfileState;
}

const ProtectedRoute = ({ profile, ...props }: IProps): React.ReactElement => {
  const { data, isLoading, isError, isSuccess } = useGetProfile();
    
  useEffect(() => {
    props.dispatch(setProfile(data));
  }, [isSuccess]);

  return (
    <Fragment>
      <div className="moderator">
        {isLoading && <span>Loading page</span>}
        {isSuccess && <Outlet />}
        {isError && <Navigate to={ROUTES.LOGIN} />}
      </div>
    </Fragment>
  );
};

const mapStateToProps = (state: RootState) => ({
  profile: getProfile(state)
});

export default connect(mapStateToProps)(ProtectedRoute);