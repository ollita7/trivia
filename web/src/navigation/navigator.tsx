import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { Login } from '../screens/login';
import ProtectedRoute from './protectedRoute'
import { ROUTES } from './constants';
import { NotFound } from '../screens/notfound';
import { Admin } from '../screens/admin';
import { Game } from '../screens/game';
import { Join } from '../screens/join';
import Layout from './layout';
import Start from '../screens/start/start';
import Leaderboard from '../screens/leaderboards/leaderboard';
import Layout2 from './layout2';
import Thanks from '../screens/thanks/thanks';
import ThanksMobile from '../screens/thanks/thanks_mobile';
import ThanksEnded from '../screens/thanks/thanks_ended';
import Reconnecting from '../screens/reconnecting/reconnecting';

const Navigator = (): React.ReactElement => {

  return (
    <Routes>
      {/* PUBLIC ROUTES */}
      <Route element={<Layout/>} >
        <Route path={ROUTES.LOGIN} element={<Login />} />
        <Route path="" element={<Login />} />
        <Route path={`${ROUTES.JOIN}/:id`} element={<Join />} />

      </Route>

      {/* PUBLIC ROUTES */}
      <Route element={<Layout2/>} >
        <Route path={ROUTES.GAME} element={<Game/>} />
        <Route path={ROUTES.LEADERBOARD} element={<Leaderboard topScore={false}/>} />
        <Route path={ROUTES.ADMIN_LEADERBOARD} element={<Leaderboard/>} />
      </Route>
      <Route path={ROUTES.ADMIN_THANKS} element={<Thanks/>} />
      <Route path={ROUTES.THANKS} element={<ThanksMobile/>} />
      <Route path={ROUTES.THANKS_ENDED} element={<ThanksEnded/>} />
      
      

      <Route element={<ProtectedRoute />}>
        <Route path={ROUTES.ADMIN_GAME} element={<Admin/>} />
        <Route path={ROUTES.ADMIN_START} element={<Start/>} />
        <Route path={ROUTES.RECONNECTING} element={<Reconnecting/>} />
      </Route>

      {/* FOR NO MATCHING ROUTES */}
      <Route path={ROUTES.WILDCARD} element={<NotFound />} />
    </Routes>
  );
};

export { Navigator };