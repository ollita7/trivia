import { ReactElement, useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist'
import { QueryClient, QueryClientProvider } from 'react-query'
import { Toaster } from 'react-hot-toast';

import store from './store/store';
import { Navigator} from './navigation';
import "../static/variables.scss";

 // Create a client
 const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
      refetchOnWindowFocus: false
    },
  },
})

let persistor = persistStore(store);


const App = (): ReactElement => {
  return (
      <QueryClientProvider client={queryClient}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
              <Navigator />
            </BrowserRouter>
          </PersistGate>
        </Provider>
          <Toaster position="bottom-center"/>
      </QueryClientProvider>
  );
};

export { App };