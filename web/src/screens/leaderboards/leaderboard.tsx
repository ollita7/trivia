import React, { ReactElement, useEffect, useState } from 'react';
import gsap from 'gsap';
import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import { connect } from "react-redux";
import { useGetTotalScores } from '../../network/services/trivia.service';
import { getPlayer } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IPlayerState } from '../../store/reducers/player';
import { useNavigate } from 'react-router-dom';
import { ROUTES } from '../../navigation/constants';
import './styles.scss'
import { useGetUser } from '../../network/services/user.service';


export interface ILeaderboardProps {
  player: IPlayerState,
  topScore?: boolean
}

const Leaderboard: React.FC<ILeaderboardProps> = ({player, topScore = true, ...props }): ReactElement => {
  const { data, isLoading, isSuccess } = useGetTotalScores();
  const profile = useGetUser();
  const navigate = useNavigate()

  //const [slide, setSlide] = useState(1)

  useEffect(() => {
    if(isSuccess && data){

      // To check if accesed by url (/admin/leaderboard)
      if(!topScore){
        setTimeout(() => {
          navigate(ROUTES.THANKS_ENDED)
        }, 65000);
      }
      
      setTimeout(() => {
        gsap.fromTo("#leaderboard .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, delay: 1.5, stagger: 0.2, onComplete: () => {

          if ($("#leaderboard-moderator").length > 0 || $("#leaderboard").length > 0 || $("#top-score").length > 0) {
  
            // Encapsula todo en una función
            function launchConfetti() {
              var duration = 15 * 2000;
              var animationEnd = Date.now() + duration;
              var defaults = { startVelocity: 30, spread: 360, ticks: 60, zIndex: 0 };
        
              function randomInRange(min, max) {
                return Math.random() * (max - min) + min;
              }
        
              var interval = setInterval(function() {
                var timeLeft = animationEnd - Date.now();
                if (timeLeft <= 0) {
                  return clearInterval(interval);
                }
        
                var particleCount = 50 * (timeLeft / duration);
                
                window?.confetti?.({ ...defaults, particleCount, origin: { x: randomInRange(0.1, 0.3), y: Math.random() - 0.2 } });
                
                window?.confetti?.({ ...defaults, particleCount, origin: { x: randomInRange(0.7, 0.9), y: Math.random() - 0.2 } });
              }, 250);
            }
        
            // Launch first time
            setTimeout(launchConfetti, 3000);
        
            // Repeat
            // setInterval(function() {
            //   launchConfetti();
            // }, 35000);
          }
          
          gsap.fromTo("#leaderboard .stag2", { opacity: 0, y: 5 }, { opacity: 1, y: 0, delay: 1.5, stagger: 0.4, onComplete: () => {
          } }, 1.5)
        }})
      }, 500);

    }
  }, [isSuccess, data])


  useEffect(() => {
    if(isSuccess && data && topScore){
      setTimeout(() => {
        let timeScroll;
        let firstSlideAnimated = false;
        let secondSlideAnimated = false;

        /* animate first Slide */
        function animateFirstSlide() {
          if (!firstSlideAnimated) {
            gsap.fromTo("#pos1", { opacity: 0, y: 10 }, { opacity: 1, y: 0, onComplete: () => {
              gsap.fromTo("#pos2", { opacity: 0, y: 10 }, { opacity: 1, y: 0, onComplete: () => {
                gsap.fromTo("#pos3", { opacity: 0, y: 10 }, { opacity: 1, y: 0 });
                /* Move to second slide */
                setTimeout(() => {
                  $('#slide2').trigger('click');
                }, 8000);
              }})
            }});
            firstSlideAnimated = true;
          }else{
            /* Move to second slide */
            setTimeout(() => {
              $('#slide2').trigger('click');
            }, 8000);

          }
        }

        /* animate second Slide */
        function animateSecondSlide() {
          console.log("SecondSlide")
          var div = $('#large-leaderboard .leaderboard');
          
          if (!secondSlideAnimated) {
            gsap.fromTo(".leaderboard-item.stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2, onComplete: () => {
              div.animate({ scrollTop: div[0].scrollHeight }, timeScroll, 'swing', function() {
                console.log('animate finish')
                div.scrollTop(0);
                setTimeout(() => {
                  $('#slide1').trigger('click');
                }, ( 500 )  );
              });
            }});
            secondSlideAnimated = true;
          } else{
            setTimeout(() => {
              div.animate({ scrollTop: div[0].scrollHeight }, timeScroll, 'swing', function() {
                console.log('animate finish')
                div.scrollTop(0);
                setTimeout(() => {
                  $('#slide1').trigger('click');
                }, ( 500 )  );
              });
            }, ( 500 )  );
          }
        }

        /* Go to slide 1 click */
        $( "#slide2" ).on( "click", function() {
          $(".title-name").html("Trivia Lineup");
          if(data.length > 5){
            timeScroll = data.length * 500
          } else {
            timeScroll = 5000
          }
          animateSecondSlide();
        } );

         /* Go to slide 2 click */
        $( "#slide1" ).on( "click", function() {
          $(".title-name").html("Trivia Podium");
          animateFirstSlide();
        } );

        $(".title-name").html("Trivia Podium");
         /* Animate first slide  */
        animateFirstSlide();

      //   $('#leaderboardCarousel').on('slid.bs.carousel', function (e) {
      //     console.log('e', e)
      //     if (e.relatedTarget.id === 'large-leaderboard') {
      //       animateSecondSlide();
      //     } else {
      //       animateFirstSlide();
      //     }
      //   });
      }, 5000);


    }
  }, [isSuccess, data, topScore])
  
  const drawScore = (data, position) => {
    if ((position-1) >= data.length) return
    return <div className={`top-score bar ${position ===1 ? "winner":""}`} id={`pos${position}`}>
            <div className="rank-box">{position}</div>
            <div className="points">
              <span className="value">{Math.ceil(data[position-1][1])}</span>
              <span className="unit">points</span>
            </div>
            <div className="divider"></div>
            <div className="leadboard-name">{data[position-1][0]}</div>
          </div>
  }

  return (
    <div id='leaderboard' className='leaderboard desktop'>
      <div className="mx-auto leaderboard-title stag">
        <h3>{!topScore && "Trivia Lineup" }</h3>
        { topScore && 
          <>
            <h3 className='title-name'>Trivia Podium</h3>
          </>
        }
        

      </div>
      {isSuccess && data && !topScore &&
        <div className="mx-auto questions stag mobile">
          <div className="col-12">
            <div className="row">
              <div className="trivia-container-wrapper">
                <div className="trivia-container-top"></div>
                <div className="leaderboard mobile trivia-container">
                  {data.map((leaderboard, index) =>
                    <>
                      <div key={index} className={`leaderboard-item ${index === 0 ? "winner":""} stag`}>
                        <div className="rank-box">{index + 1 }</div>
                        <div className="leadboard-name">{leaderboard[0]}</div>
                        <div className="divider"></div>
                        <div className="points">
                          <span className="value">{Math.ceil(leaderboard[1])}</span>
                          <span className="unit">points</span>
                        </div>
                      </div>
                      {(index === 2) &&
                        <div className="leaderboard-break" key="break"></div>
                      }
                    </>
                  )}
                </div>
              </div>
              <div className="trivia-container-bottom"></div>
            </div>
            {/* {profile &&
              <div className='row text-center align-items-center'>
                <a onClick={() => navigate(ROUTES.ADMIN_START)} className="btn btn-custom">back</a>
              </div>
            } */}
          </div>
         
        </div>
      }

      {isSuccess && data && topScore &&
        <div className="mx-auto questions stag">
          <div className="col-12">
            <div className="row">
              <div className="trivia-container-wrapper">
              <div className="trivia-container-top"></div>

                <div className="trivia-container carousel-leaderboard">

                  <div id="leaderboardCarousel" className="carousel slide" >                  
                      {/* Slides */}
                      <div className="carousel-inner">
                        <div className="carousel-item active" >
                          <div className="top-score-wrapper">
                            {drawScore(data, 2)}
                            {drawScore(data, 1)}
                            {drawScore(data, 3)}
                          </div>
                        </div>
                        <div id="large-leaderboard" className="carousel-item">
                          <div className="leaderboard">

                            {data.map((leaderboard, index) =>
                              <>
                                <div key={index} className={`leaderboard-item ${index === 0 ? "winner":""} stag`}>
                                  <div className="rank-box">{index + 1 }</div>
                                  <div className="leadboard-name">{leaderboard[0]}</div>
                                  <div className="divider"></div>
                                  <div className="points">
                                    <span className="value">{Math.ceil(leaderboard[1])}</span>
                                    <span className="unit">points</span>
                                  </div>
                                </div>
                                {(index === 2) &&
                                  <div className="leaderboard-break" key="break"></div>
                                }
                              </>
                            )}

                            </div>
                        </div>
                      </div>

                      {/* Indicators */}
                      <ol className="carousel-indicators">
                        <li id='slide1' data-bs-target="#leaderboardCarousel" data-bs-slide-to="0" className="active"></li>
                        <li id='slide2' data-bs-target="#leaderboardCarousel" data-bs-slide-to="1"></li>
                    </ol>

                  </div>
                </div>

              </div>
              <div className="trivia-container-bottom"></div>
            </div>
            {/* {profile &&
              <div className='row text-center align-items-center'>
                <a onClick={() => navigate(ROUTES.ADMIN_START)} className="btn btn-custom">back</a>
              </div>
            } */}
          </div>
         
        </div>
      }


    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(Leaderboard);