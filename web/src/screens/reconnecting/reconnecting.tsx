import React, { ReactElement,useEffect } from "react";
import { connect } from "react-redux";
import background from '../../assets/components/background.mp4';
import logo from '../../assets/img/logo.png';
import break_img from '../../assets/components/break.webp';
import ball_top from '../../assets/img/ball-top.png'
import ball_bottom from '../../assets/img/ball-bottom.png'
import "./styles.scss";

export interface IRecconectingProps {}

//TODO: need to integrate with endpoint
const Reconnecting: React.FC<IRecconectingProps> = ({ ...props }): ReactElement => {
  
    useEffect(()=>{
        gsap.fromTo("#reconnecting .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4, delay: 0.5 })
    })
  return (
    <div className="common">
        <div className="background">
            <video src={background} autoPlay loop muted playsInline></video>
        </div>
        <div className="container screen col-10 col-lg-8">
            <header>
            <div className="col-12 logo-header">
                <img src={logo} alt="ONCONNECT"/>
            </div>
            <div className="break">
                <img src={break_img} alt="ONCONNECT"/>
            </div>
            </header>
            <div id="reconnecting" className="container p-0">
                    <div className="col-12 thanks">
                    <p className="stag">Connecting to Trivia...</p>
                    <img src={break_img} />
                </div>
            </div>
        </div>
    </div>
  );
};

export default connect()(Reconnecting);
