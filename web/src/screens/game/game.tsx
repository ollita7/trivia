import React, { ReactElement, useEffect, useState } from 'react';
import { useWakeLock } from 'react-screen-wake-lock';
import { connect } from "react-redux";
import { socket } from '../../utils/socket';
import { getPlayer } from '../../store/selectors';
import { IPlayerState } from '../../store/reducers/player';
import Question from '../../components/question/question';
import MyCountdown from '../../components/countdown/countdown';
import './styles.scss'
import { useAnswer, useGetTriviaRun } from '../../network/services/trivia.service';
import { useNavigate } from 'react-router-dom';
import { ROUTES } from '../../navigation/constants';


export interface IStageProps {
  name: string;
  data: any;
}

export interface IGameProps {
  player: IPlayerState
}

const Game: React.FC<IGameProps> = ({ player, ...props }): ReactElement => {
  const [stage, setStage] = useState<IStageProps | null>(null);
  const navigate = useNavigate();
  const { data, isLoading, isSuccess } = useGetTriviaRun(player.run);
  
  socket.connect();


  const { isSupported, released, request, release } = useWakeLock({
    onRequest: () => console.log('Screen Wake Lock: requested!'),
    onError: () => console.log('An error happened 💥'),
    onRelease: () => console.log('Screen Wake Lock: released!'),
  });

  function onStartEvent(data) {
    setStage({name: 'started', data: data.message});
  }

  function onEndEvent(message) {
    socket.disconnect();
    if(player.leaderboard)
      navigate(ROUTES.LEADERBOARD);
    else
      navigate(ROUTES.THANKS)
  }

  function onCancelEvent(data) {
    socket.disconnect();
    navigate(ROUTES.THANKS_ENDED);
  }

  function onQuestionEvent(data) {
    setStage({name: 'question', data: data.message});
  }

  function onQuestionStartEvent(data) {
    setStage({name: 'question_start', data: data.message});
  }

  useEffect(() => {
    socket.on('started', onStartEvent);
    socket.on('question', onQuestionEvent);
    socket.on('ended', onEndEvent);
    socket.on('question_start', onQuestionStartEvent);
    socket.on('cancel', onCancelEvent);

    //  Request wake lock
    request()
  }, []);


  // Anims
  useEffect(() => {
    if(isSuccess && (!data || !(["running", "active"].includes(data.status)))){
      socket.disconnect();
      navigate(ROUTES.THANKS_ENDED)
    }
    if(isSuccess && data ) {
      gsap.fromTo("#game .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2 })
    }
  }, [isSuccess, data])

  return (
    <div id='game' className='game'>
      <div className="col-12 questions-title stag">
        <h4>Connect the future today trivia</h4>
      </div>
      {isLoading && 
        <div className='loading'>
          
        </div>
      }
      {
        isSuccess && data && 
        <>
          {!stage && data.status !== "running" &&
            <div className="time stag">
              <div className='get_set'>
                <span className='soon'>Get set!<br/>Trivia is about to begin</span>
              </div>
            </div>
          }
          {!stage && data.status === "running" &&
            <div className="time time-null stag">
              <div className='soon connecting'>
                <h3>Connecting to Trivia... </h3>
              </div>
              {/* <MyCountdown seconds={data.kickoff}></MyCountdown> */}
            </div>
          }
          {stage?.name == "started" &&
            <div className="time time-null stag">
              <h3>Trivia starts in</h3>
              <MyCountdown seconds={stage.data.kickoff}></MyCountdown>
            </div>
          }
          {stage?.name === 'question' && 
            <Question question={stage.data} time={player.question_time} total={data.questions}></Question> 
          }
          {stage?.name === 'question_start' && 
            <div className="col-12 col-lg-12 countdown stag">
              <p>QUESTION IN...</p>
              <MyCountdown seconds={3000}></MyCountdown>
            </div>
          }
        </>
      }
      
    </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(Game);