import React, { ReactElement, useEffect } from 'react';
import { connect } from "react-redux";
import { IStoreDispatchProps } from '../../store/storeComponent';
import background from '../../assets/components/background.mp4';
import logo from '../../assets/components/logo.webp';
import logo_footer from '../../assets/components/footer-logo.webp';
import break_img from '../../assets/components/break.webp';

import './styles.scss';

export interface IThanksProps extends IStoreDispatchProps {

}

const Thanks: React.FC<IThanksProps> = ({ ...props }): ReactElement => {
  useEffect(()=>{
    gsap.fromTo("#thanks .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4, delay: 0.5 })
  })
  return (
    <div className="moderator">
      <div className="background">
        <video src={background} autoPlay loop muted playsInline></video>       
      </div>
      <header className="container p-0 moderator">
        <div className="d-flex justify-content-between align-items-center">
            <div className="d-flex align-items-center left-side">
                <div className="mr-3 d-flex align-items-center">
                    <img src={logo} alt="ONCONNECT"/>
                </div>
                <div className="divider"></div>
                <div className="ml-3 d-flex align-items-center">
                    <h2>Connect the Future Today Trivia</h2>
                </div>
            </div>
            <div className="ml-auto d-flex align-items-center right-side">
              <img src={logo_footer} alt="AbbVie"/>
            </div>
        </div>
      </header>
      <div id="thanks" className="container p-0">
        <div className="col-12 thanks">
          <p className="stag"><span style={{ fontSize: '1em' }}>&#127881; </span> Brilliantly Played!<br/>Thanks for joining the Trivia!</p>
          <img src={break_img} />
      </div>
    </div>
    </div>
  )
}
export default connect()(Thanks);
