import React, { ReactElement, useEffect, useState } from 'react';
import gsap from 'gsap'
import { connect } from "react-redux";
import { socket } from '../../utils/socket';
import { getPlayer } from '../../store/selectors';
import { RootState } from "../../store/store";
import { useParams, useNavigate } from 'react-router-dom';
import { ROUTES } from '../../navigation/constants';
import { useGetTriviaRun } from '../../network/services/trivia.service';
import { IStoreDispatchProps } from '../../store/storeComponent';
import { IPlayerState } from '../../store/reducers/player';
import JoinForm from '../../components/join-form/join-form';
import './styles.scss'
import MyCountdown from '../../components/countdown/countdown';

export interface IJoinProps extends IStoreDispatchProps {
  player: IPlayerState
}

const Join: React.FC<IJoinProps> = ({ dispatch, player, ...props }): ReactElement => {
  const [stage, setStage] = useState<IStageProps | null>(null);
  const navigate = useNavigate();
  let { id } = useParams();
  const { data, isLoading, isSuccess } = useGetTriviaRun(id);

  socket.connect();

  useEffect(() => {
    if(isSuccess && (!data || !(["running", "active"].includes(data.status)))){
      navigate(ROUTES.THANKS_ENDED)
    }
  }, [isSuccess]);

  useEffect(() => {
    function onStartedEvent(data) {
      console.log(data)
      setStage({name: 'started', data: data.message});
    }

    function onEndEvent(data) {
      socket.disconnect();
      navigate(ROUTES.LEADERBOARD);
    }

    function onCancelEvent(data) {
      socket.disconnect();
      navigate(ROUTES.LEADERBOARD);
    }
  
    socket.on('started', onStartedEvent);
    socket.on('ended', onEndEvent);
    socket.on('cancel', onCancelEvent);
  },[])


  // Anims
  useEffect(() => {
    if(isSuccess && data ) {
      gsap.fromTo("#join .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2, delay: 0.5 })
    }
  }, [isSuccess, data])
  

  return (
    <>
      {isSuccess && data && 
        <div id='join'>
          <div className="col-12 text-login">
            <h2 className="stag">Welcome to the Connect the Future Today Trivia!</h2>
            <p className="stag">Unite with your affiliate teammates and join forces for the victory! Together, you've got this!</p>
            <p className="fw-bold stag">Let the game start!</p>
          </div>
              <JoinForm data={data}/>
              <div className="col-12 text-center">
              {stage?.name == "started" ?
                <div>
                  Game starts in 
                  <span className="fw-bold">
                    <MyCountdown seconds={stage.data.kickoff} />
                  </span>
                </div>:
                <div className='status-login stag'>
                  <p>Get set! Trivia is about to begin</p>
                </div>
              }
          </div>
        </div>
      }
      {isSuccess && !data && <div> Show message</div>}
    </>
  )
}

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(Join);