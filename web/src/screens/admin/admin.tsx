import React, { ReactElement, useState, useEffect } from 'react';
import { connect } from "react-redux";
import { socket } from '../../utils/socket';
import QRCode from "react-qr-code";
import Question from '../../components/question/question';
import MyCountdown from '../../components/countdown/countdown';
import { getPlayer } from '../../store/selectors';
import { IStoreDispatchProps } from '../../store/storeComponent';
import { RootState } from "../../store/store";
import { IPlayerState, setPlayer } from '../../store/reducers/player';
import { useNavigate } from 'react-router-dom';
import { ROUTES } from '../../navigation/constants';
import { useEnd, useGetTriviaRun, useRun } from '../../network/services/trivia.service';

import background from '../../assets/components/background.mp4';
import logo from '../../assets/components/logo.webp';
import logo_footer from '../../assets/components/footer-logo.webp';

import step1 from '../../assets/components/step-1.png';
import step2 from '../../assets/components/step-2.png';
import step3 from '../../assets/components/step-3.png';

import question_brake from '../../assets/components/question-brake.webp';

import {gsap} from 'gsap';


import './styles.scss'
import leaderboard from '../leaderboards/leaderboard';

export interface IAdminProps extends IStoreDispatchProps {
  player: IPlayerState
}

export interface IStageProps {
  name: string;
  data: any;
}

const Admin: React.FC<IAdminProps> = ({player, dispatch, ...props }): ReactElement => {
  const [stage, setStage] = useState<IStageProps | null>(null);
  const mutationEnd = useEnd();
  const mutationRun = useRun();
  
  const navigate = useNavigate();

  const handleStart = () => {
    mutationRun.mutate(player.trivia_id, {
      onSuccess: async (response) => {
        socket.connect();
        if(response){
          const newState: IPlayerState = {
            name: 'admin', 
            affiliate: null, 
            run: response.run_id,
            question_time: response.question_kickoff,
            questions: response.questions,
            trivia_id: response.id,
            time: response.kickoff,
            status: response.status,
            leaderboard: response.leaderboard
          }
          dispatch(setPlayer(newState));
        }
      },
    });
  }

  const handleCopyQR = async () => {
    await navigator.clipboard.writeText(`${window.location.origin}/join/${player.run}`)
    alert('URL copied to clipboard')
  };

  const detectKeyboard = (e) => {
    if(e.keyCode == 32) {    
      handleStart();
    }
    if(e.keyCode == 27) {
      handleCancel();
    }
  }

  const handleCancel = () => {
    mutationEnd.mutate(undefined, {
      onSuccess: async (response) => {
        socket.disconnect();
        navigate(ROUTES.ADMIN_START);
      },
    });
  }
  
  useEffect(() => {
    document.addEventListener("keyup", detectKeyboard, true);
    if(!player.run){
      socket.disconnect();
      navigate(ROUTES.LEADERBOARD)
    }
  
    function onEndEvent(data) {
      socket.disconnect();
      if(player.leaderboard)
        navigate(ROUTES.ADMIN_LEADERBOARD);
      else
        navigate(ROUTES.ADMIN_THANKS)
    }

    function onCancelEvent(data) {
      socket.disconnect();
      navigate(ROUTES.ADMIN_START);
    }
  
    function onQuestionEvent(data) {
      setStage({name: 'question', data: data.message});
    }

    function onQuestionStartEvent(data) {
      setStage({name: 'question_start', data: data.message});
    }
  
    socket.on('question', onQuestionEvent);
    socket.on('ended', onEndEvent);
    socket.on('cancel', onCancelEvent);
    socket.on('question_start', onQuestionStartEvent);

    return ()=> document.removeEventListener("keyup", detectKeyboard, true)
  }, []);

  useEffect(() => {
    // QR Screen - Moderator
    gsap.fromTo("#qr-start .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2 })

    // Countdown - Moderator
    gsap.fromTo("#countdown-moderator .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4 })

    // Questions - Moderator
    gsap.fromTo("#questions-moderator .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4 })
  }, [stage])

  return (
    <>
      <div className="background">
        <video src={background} autoPlay loop muted playsInline></video>       
      </div>
      <header className="container p-0 moderator">
        <div className="d-flex justify-content-between align-items-center">
            <div className="d-flex align-items-center left-side">
                <div className="mr-3 d-flex align-items-center">
                    <img src={logo} alt="ONCONNECT"/>
                </div>
                <div className="divider"></div>
                <div className="ml-3 d-flex align-items-center">
                    <h2>Connect the Future Today Trivia</h2>
                </div>
            </div>
            <div className="ml-auto d-flex align-items-center right-side">
              <img src={logo_footer} alt="AbbVie"/>
            </div>
        </div>
      </header>

    {!stage && 
      <>    
        <div id="qr-start" className="container qr-wrapper p-0">
          <div className="col-12 qr-start">

            <div className="time">
            {player.status === 'running' && <h3>Trivia starts in</h3> }
              <span>{player.status === 'running' && <MyCountdown seconds={player.time} stage={"starting"}></MyCountdown> }
                {player.status === 'running' && stage?.name === 'question_end' && <MyCountdown seconds={3000}></MyCountdown>}</span>
            </div>

            {player.status !== 'running' &&
              <div className="soon mt-5 mb-5">
                <span>Trivia starts soon</span>
              </div>
            }

          </div>

          <div className="col-12 ml-3">
            <div className="qr-title stag"><h3>Scan QR Code to participate</h3></div>
          </div>
          <div className="container col-6 p-0 qr-box">
            <div className="qr-image stag" onClick={handleCopyQR}><QRCode
                    size={170}
                    style={{ height: "auto" }}
                    value={`${window.location.origin}/join/${player.run}`}
                    viewBox={`0 0 170 170`}
                    /></div>
            {/* <div className="steps">
              <div className="step stag"><img src={step1}/>Open your device's camera app</div>
              <div className="step stag"><img src={step2}/>Hold your phone steady and point the camera at the QR code. Ensure it's in focus.</div>
              <div className="step stag m-0"><img src={step3}/>The camera will automatically detect the QR code and provide you with quick access to its content.</div>
            </div> */}
          </div>
        </div>
      </>
    }
    {(stage?.name === 'question' || stage?.name === 'question_end') && 
      <>        
        <div id="questions-moderator" className="container p-0 container-question">
            <div className="row">
              <div className="col-10 mx-auto">
                <div className="question">
                  <p className="stag question-title">Question <span>{stage.data.question_id+1}</span> of {player.questions.length}</p>
                  <div><div className="question-box stag"><p className="stag">{stage.data.question}</p></div>
                  <div className="text-center stag"><img src={question_brake}/></div>
                  <div className="countdown-wrapper stag">
                    <div className="countdown-circle">
                      <span className="countdown-number"><MyCountdown seconds={player.question_time}></MyCountdown></span>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </div>          

          <footer className="container p-0">
          <div className="joining-late">
          <div className="row">
            <div className="col-auto p-0">
            <QRCode
                    size={100}
                    style={{ height: "auto", border: "4px solid white" }}
                    value={`${window.location.origin}/join/${player.run}`}
                    viewBox={`0 0 58 58`}
                    />
            </div>
            <div className="col">
              <span>Joining late?</span>
              <p>Scan the QR code here and join your partners.</p>
            </div>
          </div>
        </div>
        </footer>
      </>
    }

    {stage?.name === 'question_start' && 
      <>
        <div id="countdown-moderator" className="container p-0">
          <div className="col-12 col-lg-3 countdown">
            <p className="stag">QUESTION IN...</p>
            <span className="stag"><MyCountdown seconds={3000}></MyCountdown></span>
          </div>
        </div>  

        <footer className="container p-0">
          <div className="joining-late">
          <div className="row">
            <div className="col-auto p-0">
            <QRCode
                    size={100}
                    style={{ height: "auto", border: "4px solid white" }}
                    value={`${window.location.origin}/join/${player.run}`}
                    viewBox={`0 0 58 58`}
                    />
            </div>
            <div className="col">
              <span>Joining late?</span>
              <p>Scan the QR code here and join your partners.</p>
            </div>
          </div>
        </div>
        </footer>
      </>
    }
    </>
  )
}

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(Admin);