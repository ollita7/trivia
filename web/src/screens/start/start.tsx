import React, { ReactElement, useState, useEffect } from 'react';
import { useGetTrivias } from '../../network/services/trivia.service';
import { useStart, useCleanup } from '../../network/services/trivia.service';

import './styles.scss'
import { IPlayerState, setPlayer } from '../../store/reducers/player';
import { IStoreDispatchProps } from '../../store/storeComponent';
import { useNavigate } from 'react-router-dom';
import { ROUTES } from '../../navigation/constants';
import { connect } from 'react-redux';

import background from '../../assets/components/background.mp4';
import background_moderator from '../../assets/components/background-moderator.png';
import logo from '../../assets/img/logo.svg';
import logo_footer from '../../assets/components/footer-logo.webp';
import {gsap} from 'gsap';

export interface IStartProps extends IStoreDispatchProps {
}

const Start: React.FC<IStartProps> = ({dispatch, ...props }): ReactElement => {
  const navigate = useNavigate();
  const [trivia, setTrivia] = useState<number | null>(null); // null for non selected option at start
  const { data, isLoading, isSuccess } = useGetTrivias();
  const mutation = useStart();
  const cleanmutation = useCleanup();

  useEffect(() => {
    if(data){
      // Commented for non initial selected in selection
      // setTrivia(data[0].id)
      // Question sets - Moderator
      gsap.fromTo("#questions-sets .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2, onComplete: () => {
        gsap.fromTo("#questions-sets .stag2", { opacity: 0, y: 40 }, { opacity: 1, y: 0, stagger: 0.4, onComplete: () => {
          gsap.fromTo("#questions-sets .stag3", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4 })
          gsap.to("#questions-sets .day", { pointerEvents: 'all'  })
        }})
      }})
    }
  }, [data]);

  const handlePlay = () => {
    if (trivia) {
      mutation.mutate(trivia, {
        onSuccess: async (response) => {
          if(response){
            const newState: IPlayerState = {
              name: 'admin', 
              affiliate: null, 
              run: response.run_id,
              question_time: response.question_kickoff,
              questions: response.questions,
              trivia_id: response.id,
              time: response.kickoff,
              status: null,
              leaderboard: response.leaderboard
            }
            dispatch(setPlayer(newState));
            navigate(ROUTES.ADMIN_GAME);
          }
        },
      });
    }
  }

  const handleCleanup = () => {
  
      cleanmutation.mutate(undefined, {
        onSuccess: async (response) => {          
          alert('cleanup complete');
        },
      });
    
  }

  const handleChangeTrivia = (id) => {
    setTrivia(id)
  }

  return (
    <>
      <div className="background">
        <video src={background} autoPlay loop muted playsInline></video>
        <img src={background_moderator}/>
      </div>
      {/* HEADER */}  
      <header className="container p-0 questions-select">
        <div className="d-flex justify-content-center align-items-center" style={{height: 100}}>
            <div className="d-flex flex-grow-1 justify-content-center align-items-center">
                <div className="mr-3 d-flex align-items-center">
                    <img src={logo} alt="ONCONNECT" />
                </div>
                <div className="divider"></div>
                <div className="ml-3 d-flex align-items-center">
                    <h3>Connect the Future Today Trivia</h3>
                </div>
            </div>
        </div>
      </header>
      {(isSuccess) && 
        <div id="questions-sets" className="container p-0 sets-container">
          <div className="title stag">Please select the day for the trivia</div>
          <div className="sets">
            <div className="row g-5">
              {data.map((t, index) => 
                <div className="col-4" key={t.id} onClick={() => handleChangeTrivia(t.id)}>
                  <div className={`day ${t.id == trivia ? "selected": ""} stag2`}>
                    <span>{t.name}</span>
                    <ul className='question_list'>
                      {t.questions.map(q =>
                        <li key={q.id}>{q.question}</li>
                      )}
                    </ul>
                  </div>
              </div>
              )}
              
            </div>
          </div>
          <div className="col-12 button-start stag3"><button className="col-4" onClick={() => handlePlay()}>Let's get started</button></div>
        </div>
        }

      <div className="col-12 button-start stag3" style={{display: 'none', justifyContent: "center"}}><button className="col-2 btn btn-primary" onClick={() => handleCleanup()}>cleanup</button></div>

      <footer className="container p-0 mt-5">
        <div className="row">
            <div className="col d-flex align-items-center justify-content-center">
                <div className="flex-grow-1 border"></div>
                <div className="logo-image">
                  <img src={logo_footer} alt="AbbVie" />
                </div>
                <div className="flex-grow-1 border"></div>
            </div>
        </div>
    </footer>
    </>
  )
}

export default connect()(Start);