import React, { ReactElement, useState } from 'react';
import { connect } from "react-redux";
import { useNavigate } from 'react-router-dom';
import { IStoreDispatchProps } from '../../store/storeComponent';
import { ROUTES } from '../../navigation/constants';
import { Config } from '../../utils';
import { ILogin, useLogin } from '../../network/services/user.service';

import './styles.scss'
export interface ILoginProps extends IStoreDispatchProps {

}

const Login: React.FC<ILoginProps> = ({ ...props }): ReactElement => {
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();
  const mutation = useLogin();

  const handleContinue = (event) => {
    event.prevent
    let values: ILogin = { username: user, password: password}
    mutation.mutate(values, {
      
      onSuccess: async (response) => {
        localStorage.setItem(Config.USER, JSON.stringify(response));
        navigate(ROUTES.ADMIN_START);
      },
    });
  }

  return (
    <>
      <div className="col-12 text-login">
        <h2 className="stag">Welcome to the Connect the Future Today Trivia!</h2>
        <p className="fw-bold stag">Please login!</p>
      </div>
      <form className="w-100 needs-validation" noValidate>
        <div className="position-relative stag">
        <input type="text" className="form-login" value={user} onChange={event => setUser(event.target.value)} id="email" placeholder="Username" required/>
          <div className="invalid-tooltip">
            Please enter your Username.
          </div>
        </div>
        <div className="position-relative stag">
          <input type="password" className="form-login" id="password" value={password} onChange={event => setPassword(event.target.value)} placeholder="Password" required/>
          <div className="invalid-tooltip">
            Please enter your password.
          </div>
        </div>
        
        <div className="btn-wrapper w-100 stag">
          <button type="button" onClick={handleContinue} className="btn btn-custom">Login</button>
        </div>
      </form>
    </>
  )
}
export default connect()(Login);
