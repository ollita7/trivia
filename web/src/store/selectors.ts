import { createSelector } from '@reduxjs/toolkit';
import { IPlayerState } from './reducers/player';
import { IProfileState } from './reducers/profile';
import { RootState } from './store';

export const getPlayer = createSelector([(store: RootState): IPlayerState => store.player], player => player);
export const getProfile = createSelector([(store: RootState): IProfileState => store.profile], profile => profile);