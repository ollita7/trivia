import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface IPlayerState {
  name: string | null;
  affiliate: string | null;
  run: string | undefined;
  question_time: number;
  questions: Array<any>;
  time?: number;
  trivia_id: number;
  leaderboard: boolean;
  status: string | null;
}

const initialState: IPlayerState = {
  name: null,
  affiliate: null,
  run: undefined,
  question_time: 0,
  questions: [],
  time: 0,
  trivia_id: 0,
  status: null,
  leaderboard: true
}

const playerSlice = createSlice({
  name: 'player',
  initialState,
  reducers: {
    setPlayer: (state, action: PayloadAction<IPlayerState>) => {
      const newState = {...state, ...action.payload };
      return newState;
    },
    clearPlayer: state => initialState,
  },
});

export default playerSlice.reducer;
export const { setPlayer} = playerSlice.actions;
