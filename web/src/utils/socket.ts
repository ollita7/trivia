import { io } from 'socket.io-client';
import { Config } from './config';

const origin = new URL(Config.API_URL).origin;

export const socket = io(origin, {
  path: '/socket',
  autoConnect: false, 
  transports: ['websocket']
});