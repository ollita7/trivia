import React, { ReactElement, useState, useEffect } from 'react';
import { connect } from "react-redux";
import { getPlayer } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IPlayerState, setPlayer } from '../../store/reducers/player';
import { useGetAffiliates } from '../../network/services/trivia.service';
import { IStoreDispatchProps } from '../../store/storeComponent';
import { ROUTES } from '../../navigation/constants';
import { useNavigate, useParams } from 'react-router-dom';

export interface IJoinFormProps extends IStoreDispatchProps {
  player: IPlayerState;
  data: any
}

const JoinForm: React.FC<IJoinFormProps> = ({ dispatch, player, data, ...props }): ReactElement => {
  const [name, setName] = useState(player.name);
  const navigate = useNavigate();
  let { id } = useParams();
  const [affiliate, setAffiliate] = useState(player.affiliate);
  const {data: affiliates} = useGetAffiliates();

  const handleContinue = () => {
    if(!name || !affiliate)
      return; 
    const newState: IPlayerState = {
      name: name, 
      affiliate: affiliate, 
      run: id,
      question_time: data.question_kickoff,
      trivia_id: data.id,
      status: data.status,
      leaderboard: data.leaderboard
    }
    dispatch(setPlayer(newState));
    navigate(ROUTES.GAME);
  }

  const handleChangeAffiliate = (e) => {
    setAffiliate(e.target.value)
  }

  return (
    <form className="w-100 needs-validation" noValidate>
      <div className="position-relative stag">
        <input type="text" className="form-login" id="name" name='firstName' autoComplete='on' value={name} onChange={event => setName(event.target.value)} placeholder="Write your name here…" required/>
        <div className="invalid-tooltip">
          Please enter your name.
        </div>
      </div>
      <div className="position-relative stag">
        <select defaultValue={affiliate} className="form-select-login" id="affiliate" onChange={event => handleChangeAffiliate(event)} required>
          <option value="" key="a" disabled selected={true}>Select your affiliate</option>
          {affiliates && affiliates.map(a => 
            <option key={a.name} value={a.name} selected={a.name == affiliate}>{a.name}</option>  
          )}
          <option key="OTHER" value="OTHER">OTHER</option>  
        </select>
        <div className="invalid-tooltip">
          Please select an affiliate.
        </div>
      </div>
      
      <div className="btn-wrapper w-100 stag">
        <button type="button" onClick={handleContinue} className="btn btn-custom">Next</button>
      </div>
      
    </form>
    
  )
}

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(JoinForm);