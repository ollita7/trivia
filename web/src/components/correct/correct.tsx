import React, { ReactElement, useState, useEffect } from 'react';
import { connect } from "react-redux";
import { getPlayer } from '../../store/selectors';
import { RootState } from "../../store/store";
import { socket } from '../../utils/socket';
import { IPlayerState } from '../../store/reducers/player';

export interface IStageProps {
  name: string;
  data: any;
}

export interface ICorrectProps {
  player: IPlayerState;
}

const Correct: React.FC<ICorrectProps> = ({ player, ...props }): ReactElement => {
  const [correct, setCorrect] = useState<IStageProps | null>(null);

  useEffect(() => {
    function onQuestionEndEvent(data) {
      setCorrect({name: 'question_end', data: data.message});
    }
  
    socket.on('question_end', onQuestionEndEvent);
    socket.on('question', ()=> setCorrect(null));
  }, []);

  const showCorrect = (data) => {
    let correct = null;
    data.answer.forEach(answer => {
      if (answer[data.solution])
        correct = answer[data.solution];
    });
    return correct;
  }

  return (
    <>
      {correct &&
        <div className='col-12'>Correct answer is: {showCorrect(correct.data)}</div>
      }
    </>
    
  )
}

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(Correct);