import React, { ReactElement, useState, useEffect } from 'react';
import Countdown from 'react-countdown';
import $ from 'jquery'
import './styles.scss'

export interface ICountdownProps {
  seconds: number;
  stage: any;
}

const renderer = ({ hours, minutes, seconds, completed }) => {
  if (completed) {
    return <></>;
  } else {
    return <span>{minutes > 0 && <>{minutes.toString().padStart(2, "0")}:</>}{seconds.toString().padStart(2, "0")}</span>;
  }
};

const MyCountdown: React.FC<ICountdownProps> = ({ seconds, stage, ...props }): ReactElement => {

  useEffect(() => {
    if(stage === "starting"){
      // let i = setInterval(() => {
      //   console.log('text', $('.mycountdown span').text())
      //     if($('.mycountdown span').text() == '04'){
      //       alert("llego")
      //     }
      // }, 1000);
    }
  }, [stage])
  

  return (
    <div className='mycountdown'>
      <Countdown date={Date.now() + seconds } renderer={renderer}/>
    </div>
  )
}

export default MyCountdown;