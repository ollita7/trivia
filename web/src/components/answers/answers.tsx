import React, { ReactElement, useState, useEffect } from 'react';
import gsap from 'gsap';
import { connect } from "react-redux";
import { getPlayer } from '../../store/selectors';
import { RootState } from "../../store/store";
import { IAnswer, useAnswer } from '../../network/services/trivia.service';
import { IPlayerState } from '../../store/reducers/player';
import check from '../../assets/components/check.webp';
import cross from '../../assets/components/cross.webp';
import $ from 'jquery'
import './styles.scss'

export interface IAdminProps {

}

export interface IQuestion {
  answer: any[];
  question: string;
  question_id: string;
}

export interface IQuestionProps {
  question: IQuestion;
  showAnswers?: boolean
  player: IPlayerState;
}

const Answers: React.FC<IQuestionProps> = ({ player, question, showAnswers=true, ...props }): ReactElement => {
  const [selectedAswer, setSelectedAswer] = useState<any | null>(null);

  const mutation = useAnswer();

  useEffect(() => {
    setSelectedAswer(null);
  }, [question]);


  useEffect(() => {
    setTimeout(() => {
      gsap.fromTo("#questions .stag2", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2 })
    }, 1000);
  }, [question.answer])
  

  useEffect(() => {
    gsap.fromTo("#questions .stag3", { opacity: 0, y: 10 }, { opacity: 1, y: 0, delay: 1.5, stagger: 0.2 })
  }, [selectedAswer])
  

  const handleAnswer = (question_id, answer) => {
    if(!showAnswers || selectedAswer)
      return;
    let values: IAnswer = {
      triviaId: player.trivia_id,
      answer: answer,
      runId: player.run || '',
      questionId: question_id,
      playerName: player.name || '',
      group: player.affiliate || '',
      responseTime: Date.now().toString(),
    }

    mutation.mutate(values, {
      onSuccess: async (response) => {
        if(response?.correctAnswer == answer){
          confettiCorrectAnswer()
        }
        setSelectedAswer({answer: answer, correctAnswer:response?.correctAnswer, correct: response?.correctAnswer == answer});
      },
    });
  }

  const answerStyle = (key) => {
    if (key === selectedAswer?.correctAnswer || (key === selectedAswer?.answer && selectedAswer.correct))
      return 'opt-selected correct';
    else if (key === selectedAswer?.answer && !selectedAswer.correct)
      return 'opt-shake incorrect';
    else if (selectedAswer)
      return 'disabled'
  } 
  

  const confettiCorrectAnswer = () => {
      // Confeti Correct Answer Script
      console.log('correct anwser')

      var count = 200;
      var defaults = {
        origin: { y: 0.7 }
      };
  
      function fire(particleRatio, opts) {
        window?.confetti?.({
          ...defaults,
          ...opts,
          particleCount: Math.floor(count * particleRatio)
        });
      }
  
      fire(0.25, {
        spread: 26,
        startVelocity: 55,
      });
      fire(0.2, {
        spread: 60,
      });
      fire(0.35, {
        spread: 100,
        decay: 0.91,
        scalar: 0.8
      });
      fire(0.1, {
        spread: 120,
        startVelocity: 25,
        decay: 0.92,
        scalar: 1.2
      });
      fire(0.1, {
        spread: 120,
        startVelocity: 45,
      });
      
  }

  return (
      <div className="answers-box">
        <div className="row">
          {question.answer.map(answer => 
            Object.keys(answer).map((key) => 
              <button className={`answer-option stag2 ${answerStyle(key)}`} key={key} 
                    onClick={()=> { handleAnswer(question.question_id, key) }}>
                {answer[key]}
              </button>
            ))
          }
        </div>
        {selectedAswer &&
          <>
          {selectedAswer.correct ?
            <div id="feedback-message" className="feedback correct col-10 col-lg-4 stag3">
              <div className="feedback-wrapper">
                <p><img src={check}/>YOU'RE RIGHT!</p>
                <span>Hang on for others to answer</span>
              </div>
            </div>:
            <div id="feedback-message" className="feedback incorrect col-10 col-lg-4 stag3">
              <div className="feedback-wrapper">
                <p><img src={cross}/>NICE TRY!</p>
                <span>Hang on for others to answer</span>
              </div>
            </div>
          }
          </>
        }
      </div>
  )
}

const mapStateToProps = (state: RootState) => ({
  player: getPlayer(state),
});

export default connect(mapStateToProps)(Answers);