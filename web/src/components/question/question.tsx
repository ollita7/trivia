import React, { ReactElement, useState, useEffect } from 'react';
import gsap from 'gsap';
import MyCountdown from '../../components/countdown/countdown';
import Answers from '../answers/answers';
import './styles.scss'
import Correct from '../correct/correct';

export interface IAdminProps {

}

export interface IQuestion {
  answer: any[];
  question: string;
  question_id: string;
}

export interface IQuestionProps {
  question: IQuestion;
  total: number;
  time: number;
  onClick?: any;
  showAnswers?: boolean
}

const Question: React.FC<IQuestionProps> = ({ question, total, time, onClick, showAnswers=true, ...props }): ReactElement => {

  // Anims
  useEffect(() => {
      gsap.fromTo("#questions .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2 })
  }, [])
  

  return (
    <div id="questions" className='col-12 questions_answer'>
      <div className="question-box stag">
        <div className="countdown-wrapper stag">
          <div className="countdown-circle">
            <span className="countdown-number"><MyCountdown key={question.question_id} seconds={time}></MyCountdown></span>
          </div>
        </div>
        <div className="question-content stag">
          <p className="question-position">QUESTION <span className="current-position">{question.question_id+1}</span> OF {total}</p>
          <h2 className="question-text">{question.question}</h2>
        </div>
      </div>
      {showAnswers &&
        <Answers question={question} showAnswers={showAnswers}></Answers>
      }
      {!showAnswers && <Correct></Correct>}
    </div>
  )
}

export default Question;