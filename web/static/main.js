// GSAP Animations
$(document).ready(function(){
  // Countdown
  gsap.fromTo("#countdown .stag", { opacity: 0, y: 5 }, { opacity: 1, y: 0, stagger: 0.4 })

  // Leaderboard
  gsap.fromTo("#leaderboard .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2, onComplete: () => {
    gsap.fromTo("#leaderboard .stag2", { opacity: 0, y: 5 }, { opacity: 1, y: 0, stagger: 0.4 }, 1.5)
  }})

  // Login
  gsap.fromTo("#login .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2, onComplete: () => {
    gsap.fromTo("#leaderboard .stag2", { opacity: 0, y: 5 }, { opacity: 1, y: 0, stagger: 0.4 }, 1.5)
  }})

  // Question
  gsap.fromTo("#questions .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2, onComplete: () => {
    gsap.fromTo("#questions .stag2", { opacity: 0, y: 5 }, { opacity: 1, y: 0, stagger: 0.4 }, 1.5)
  }})

  // Question Feedback
  gsap.fromTo("#feedback-message .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2 })

  // Question sets - Moderator
  gsap.fromTo("#questions-sets .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2, onComplete: () => {
    gsap.fromTo("#questions-sets .stag2", { opacity: 0, y: 40 }, { opacity: 1, y: 0, stagger: 0.4, onComplete: () => {
      gsap.fromTo("#questions-sets .stag3", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4 })
    }}, 1.5)
  }})

  // QR Screen - Moderator
  gsap.fromTo("#qr-start .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2 })

  // Countdown - Moderator
  gsap.fromTo("#countdown-moderator .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4 })

  // Questions - Moderator
  gsap.fromTo("#questions-moderator .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4 })

  // Thanks - Moderator
  gsap.fromTo("#thanks .stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.4, delay: 0.5 })

  // Top Score and Top Score - Moderator
    // Flag to control the animation for each slide
    let firstSlideAnimated = false;
    let secondSlideAnimated = false;

    // Animation for the first slide
    function animateFirstSlide() {
      if (!firstSlideAnimated) {
        gsap.fromTo("#pos3", { opacity: 0, y: 10 }, { opacity: 1, y: 0, onComplete: () => {
          gsap.fromTo("#pos2", { opacity: 0, y: 10 }, { opacity: 1, y: 0, onComplete: () => {
            gsap.fromTo("#pos1", { opacity: 0, y: 10 }, { opacity: 1, y: 0 });
          }})
        }});
        firstSlideAnimated = true;
      }
    }

    // Animation for the second slide
    function animateSecondSlide() {
      if (!secondSlideAnimated) {
        gsap.fromTo(".leaderboard-item.stag", { opacity: 0, y: 10 }, { opacity: 1, y: 0, stagger: 0.2 });
        secondSlideAnimated = true;
      }
    }

    // Execute the animation for the first slide initially
    animateFirstSlide();

    // Listen to Bootstrap carousel events
    $('#leaderboardCarousel').on('slid.bs.carousel', function (e) {
      if (e.relatedTarget.id === 'large-leaderboard') {
        // Run the animation for the second slide if it hasn't been animated
        animateSecondSlide();
      } else {
        // Run the animation for the first slide if it hasn't been animated
        animateFirstSlide();
      }
    });


  
});


// Example starter JavaScript for disabling form submissions if there are invalid fields
(() => {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }

      form.classList.add('was-validated')
    }, false)
  })
})()

// Auto scroll in carosuel
$(document).ready(function(){
$('#leaderboardCarousel').on('slide.bs.carousel', function (e) {
  var nextId = $(e.relatedTarget).attr('id');
  if (nextId === 'large-leaderboard') {
    setTimeout(function(){
      var div = $('#large-leaderboard .leaderboard');
      div.animate({ scrollTop: div[0].scrollHeight }, 4000, 'swing', function() {
        setTimeout(function(){
          div.animate({ scrollTop: 0 }, 4000, 'swing'); 
        }, 2500);
      });
    }, 3000);
  }
});
});

// Confeti
$(document).ready(function(){
if ($("#leaderboard-moderator").length > 0 || $("#leaderboard").length > 0 || $("#top-score").length > 0) {

  // Encapsula todo en una función
  function launchConfetti() {
    var duration = 15 * 2000;
    var animationEnd = Date.now() + duration;
    var defaults = { startVelocity: 30, spread: 360, ticks: 60, zIndex: 0 };

    function randomInRange(min, max) {
      return Math.random() * (max - min) + min;
    }

    var interval = setInterval(function() {
      var timeLeft = animationEnd - Date.now();
      if (timeLeft <= 0) {
        return clearInterval(interval);
      }

      var particleCount = 50 * (timeLeft / duration);
      confetti({ ...defaults, particleCount, origin: { x: randomInRange(0.1, 0.3), y: Math.random() - 0.2 } });
      confetti({ ...defaults, particleCount, origin: { x: randomInRange(0.7, 0.9), y: Math.random() - 0.2 } });
    }, 250);
  }

  // Launch first time
  setTimeout(launchConfetti, 1000);

  // Repeat
  setInterval(function() {
    launchConfetti();
  }, 35000);
}
});

// Confeti Correct Answer
$(document).ready(function(){
if ($("#questions").length > 0) {
  // Confeti Correct Answer Script
  var count = 200;
  var defaults = {
    origin: { y: 0.7 }
  };

  function fire(particleRatio, opts) {
    confetti({
      ...defaults,
      ...opts,
      particleCount: Math.floor(count * particleRatio)
    });
  }

  fire(0.25, {
    spread: 26,
    startVelocity: 55,
  });
  fire(0.2, {
    spread: 60,
  });
  fire(0.35, {
    spread: 100,
    decay: 0.91,
    scalar: 0.8
  });
  fire(0.1, {
    spread: 120,
    startVelocity: 25,
    decay: 0.92,
    scalar: 1.2
  });
  fire(0.1, {
    spread: 120,
    startVelocity: 45,
  });
}
});
  
// Keyboard
// window.addEventListener('keyup', function(event) {
//   if (event.keyCode == 32) {
//     alert('Espacio')
//   }
//   if (event.keyCode == 76) {
//     alert('ELE')
//   }
// });