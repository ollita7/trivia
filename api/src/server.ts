import { createKiwiServer, IKiwiOptions, AuthorizeResponse, getSocket } from 'kiwi-server';
import * as http from 'http';
import { TriviaController } from './controllers/trivia.controller'
import { UserController } from './controllers/user.controller';
import { UserService } from "./services/user.service";
import { HeadersMiddleware } from "./middlewares/headers";
import { AffiliateController } from './controllers/affiliate.controller';

async function validateAuthentication(
    request: any,
    roles: Array<string>
  ): Promise<AuthorizeResponse | boolean> {
    const token = request.headers["authorization"];
    if (!token) {
      return new AuthorizeResponse(401, "User is not authenticated");
    }
    const userService = new UserService();
    const profile = userService.getProfile(token);
    if (!profile) return new AuthorizeResponse(401, "Invalid token");
  
    request["userProfile"] = {
      name: profile.name,
      email: profile.email,
      picture: profile.picture ? profile.picture : null,
      account: request.headers['account'],
      project: request.headers['project']
    }
    return true
  }

const options: IKiwiOptions = {
    controllers: [TriviaController, UserController, AffiliateController],
    authorization: validateAuthentication,
    middlewares: [HeadersMiddleware],
    cors: {
      enabled: true,
      domains: [
          "http://localhost:8080",
          "https://trivia.cavepot.com",
          "http://trivia.onconnect23.com",
          "https://trivia.onconnect23.com"
      ]
    },
    documentation: {
        enabled: true,
        path: '/apidoc'
    },
    log: true,
    port: parseInt(process.env.PORT),
    prefix: "/v1",
    socket: {
      path: '/socket',
      enabled: true,
      cors: [
        'http://localhost:8080',
        "https://trivia.cavepot.com"
      ]
    },
}

const server = createKiwiServer(options, socketInit);

function socketInit() {
  const io = getSocket();
  io.on('connection', (socket: any) => {
    socket.userId  = socket.handshake.query.user;
  });

  io.engine.on("connection_error", (err: any) => {
    console.log(err.req);      // the request object
    console.log(err.code);     // the error code, for example 1
    console.log(err.message);  // the error message, for example "Session ID unknown"
    console.log(err.context);  // some additional error context
  });
}