import { JsonController, Get, Post, Delete, Param, getSocket, Authorize, Body} from 'kiwi-server';
import { TriviaService } from '../services/trivia.service';
import { ResponseCode, TriviaRunStatus } from '../sdk/constants';
import { Response } from '../sdk/response';
import { AnswerService } from '../services/answer.service';
import { AnswerIn } from '../models/answer.model';

@JsonController('/trivia')
export class TriviaController {

    constructor(private triviaService: TriviaService, private answerService: AnswerService){
    }  

    @Get('/total_scores')
    public async getTotalScores(){     
        let result = await this.triviaService.totalScores(["GLOBAL"]);   
        return new Response(ResponseCode.OK,'',result);
    }

    @Get('/connected')
    public async connected(){     
      const io = getSocket();
      let result = { 
        total: io.sockets.sockets.size,
        clients: Array.from(io.sockets.sockets, ([name, value]) => ({ name}))
      };
      return new Response(ResponseCode.OK,'', result);
    }
    
    @Authorize()
    @Delete('/cleanup')
    public async cleanup(){     
        let result = await this.triviaService.cleanup();   
        return new Response(ResponseCode.OK,'',result);
    }
    
    @Get('/:id')
    public async getTrivia(@Param('id') id: string){    
      let result = await this.triviaService.get(id); 
      return new Response(ResponseCode.OK,'',result);
    }

    @Authorize()
    @Get('')
    public async getTrivias(){    
      let result = await this.triviaService.list(); 
      return new Response(ResponseCode.OK,'',result);
    }

    @Authorize()
    @Post('/:id/start')
    public async startTrivia(@Param('id') id: string){    
      //TODO: Create new RUN - Check if not in progress.
      let result = await this.triviaService.startTrivia(id);   
      if(!result) {
        return new Response(ResponseCode.ERROR,'Trivia already active',result);
      }  
      return new Response(ResponseCode.OK,'',result);
    }

    @Authorize()
    @Post('/:id/run')
    public async runTrivia(@Param('id') id: string){         
      let result = await this.triviaService.runTrivia(id);   
      if(!result) {
        return new Response(ResponseCode.ERROR,'Trivia already running',result);
      }  
      return new Response(ResponseCode.OK,'',result);
    }

    @Authorize()
    @Post('/end')
    public async endTrivia(){   
        let result = await this.triviaService.cancelTrivia([]);   
        if(!result) {
            return new Response(ResponseCode.OK,'Trivia was cancelled sucessfully',result);
        }  
        return new Response(ResponseCode.OK,'',result);
    }
  
    @Authorize()
    @Get('/name/:name')
    public async getTriviaByName(@Param('name') name: string){    
      let result = await this.triviaService.getByName(name); 
      return new Response(ResponseCode.OK,'',result);
    }

    @Get('/run/:id')
    public async getRun(@Param('id') id: string){    
      let result = await this.triviaService.getRun(id); 
      if (!result)
        return new Response(ResponseCode.ERROR,'Trivia is not found or not started', result);
      if (![TriviaRunStatus.ACTIVE, TriviaRunStatus.RUNNING].includes(result.status))
        return new Response(ResponseCode.ERROR,'Trivia is not active', result);
      return new Response(ResponseCode.OK,'',result);
    }

    @Authorize()
    @Get('/:id/question/:index')
    public async getQuestionByPosition(@Param('id') id: string, @Param('index') index: number){    
        let result = await this.triviaService.get(id);
        let question;
        if(result && result.questions.length >= index){
            question = result.questions[index];
        }else{
            return new Response(ResponseCode.ERROR,'No question in that position',question);
        }
        return new Response(ResponseCode.OK,'',question);
    }

    @Post('/answer')
    public async answerQuestion(@Body() answer: AnswerIn){        
        let result = await this.answerService.addAnswer(answer);   
        if(!result) {
            return new Response(ResponseCode.OK,'Cannot add answer',result);
        }  
        return new Response(ResponseCode.OK,'',result);
    }

    @Get('/:run_id/results')
    public async getTriviaScores(@Param('run_id') run_id: string){    
        //TODO: rethink how we can get total scores    
        let run = await this.triviaService.getRun(run_id); 
        return new Response(ResponseCode.OK,'', run.results);
    }

    //DELETE
    @Get('/calculate/:run_id')
    public async getCalculateScores(@Param('run_id') run_id: string){     
        let result = await this.triviaService.calculateScores(run_id);   
        return new Response(ResponseCode.OK,'',true);
    }



}