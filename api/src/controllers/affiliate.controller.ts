import { JsonController, Get} from 'kiwi-server';
import { AffiliateService } from '../services/affiliate.service';
import { ResponseCode } from '../sdk/constants';
import { Response } from '../sdk/response';

@JsonController('/affiliate')
export class AffiliateController {

    constructor(private affiliateService: AffiliateService){
    }  
   
    @Get('')
    public async getAffiliates(){    
      let result = await this.affiliateService.list(); 
      return new Response(ResponseCode.OK,'',result);
    }

}