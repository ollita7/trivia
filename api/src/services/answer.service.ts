
import { getRepository } from '../datastore';
import { Trivia } from '../datastore/entities';
import { AnswerIn } from '../models/answer.model';
import { Answer, Run } from '../datastore/entities';
const {ObjectID} = require('mongodb');
import { getSocket } from 'kiwi-server';
import { TriviaRunStatus } from '../sdk/constants';

export class AnswerService {

  async addAnswer(answer: AnswerIn) {
    try{
      answer.responseTime =   new Date().getTime().toString();  
      let answerRepo = await getRepository(Answer); 
      let runRepo = await getRepository(Run);
      let currentRun = await runRepo.findOne({ where: {  _id: new ObjectID(answer.runId)  } })
      if(currentRun && currentRun.status == TriviaRunStatus.RUNNING){
        const result = await answerRepo.insert(answer); 
        let totalAnswers = await answerRepo.find({where: {  runId: answer.runId, questionId: answer.questionId  }});
        totalAnswers = totalAnswers.length;

        this.sendMessage(totalAnswers, "answers");
        console.log(totalAnswers, "answers")
        let triviaRepo = await getRepository(Trivia);
        let trivia = await triviaRepo.findOne( { where: {  'id': currentRun.id.toString()  } });
        let correctAnswer = trivia.questions[+answer.questionId].solution;
        
        return {totalAnswers, correctAnswer};
      }
      else {
        return null;
      }
    }catch (ex) {
      return null;
    }

  }


  sendMessage(m: string, e:string) {
    try{
      const io = getSocket();
      io.timeout(50000).emit(e, {message: m}, (err: any, responses: any) => {
        if (err) {
          console.log(`ERROR: ${err}`)
        }
      });
      return true;
    }catch (ex) {
      return null;
    }
  }


}