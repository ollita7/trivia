
import { TriviaRunStatus } from '../sdk/constants';
import { getRepository } from '../datastore';
import { Answer, Question, Run, Trivia } from '../datastore/entities';
const {ObjectID} = require('mongodb');
import { getSocket } from 'kiwi-server';
export class TriviaService {
  
  async get(id: string) {    
      try{
        let repo = await getRepository(Trivia)
        let trivia = await repo.findOne({ where: { id:id.toString() } })
        return trivia
      }catch (ex) {
        return null;
      }

  }

  async getRun(id: string) {    
    try{
      let repo = await getRepository(Run)
      let run = await repo.findOne({ where: { _id:  new ObjectID(id) } })
      if(run.status === TriviaRunStatus.RUNNING){
        const diffInMs = Math.abs(new Date().getTime() - new Date(run.start_time).getTime());
        run['kickoff'] = diffInMs;  
      }
      run['question_kickoff'] = +process.env.QUESTION_TIME;
      return run
    }catch (ex) {
      return null;
    }

  }

  async list() {
    try{
      let repo = await getRepository(Trivia)
      let trivias = await repo.find()
      return trivias
    }catch (ex) {
      return null;
    }

  }

  async getByName(name: string) {
    try{
      let repo = await getRepository(Trivia)
      let trivia = await repo.findOne({ where: { name } })
      return trivia
    }catch (ex) {
      return null;
    }

  } 

  async startTrivia(triviaId: string) {
    try{
      let result = await this.get(triviaId); 
      let runRepo = await getRepository(Run);
      let currentRun = await runRepo.findOne({status: TriviaRunStatus.RUNNING });
      if(currentRun) {
        return false;
      }     
      else {
        let currentRun = await runRepo.findOne({status: TriviaRunStatus.ACTIVE });
        if (currentRun){
          let updateResult = await runRepo.findOneAndUpdate(
            { _id: new ObjectID(currentRun._id) },
            { $set: { end_time: new Date().getTime().toString(), status: TriviaRunStatus.CANCELED, question_times: null } }
            ); 
        };

        let run = new Run();
        run.id = triviaId;
        run.leaderboard = result.leaderboard;
        run.status = TriviaRunStatus.ACTIVE;
        run.start_time = null;
        run.end_time = null;
        run.results = null;
        run.questions = result.questions.length;
        const insertResult = await runRepo.insert(run); 
        result['run_id'] = insertResult.raw['insertedId']; 
        if(!insertResult){
          return false;
        }
        return result
      }
    }catch (ex) {      
      return null;
    }
  } 

  async runTrivia(triviaId: string) {
    try{
      let result = await this.get(triviaId); 
      let runRepo = await getRepository(Run);
      let currentRun = await runRepo.findOne({ where: { id: triviaId, status: TriviaRunStatus.ACTIVE } });
      let questiontimes: Array<string> = [];
      if(!currentRun) {
        return false;
      }else {
        let start_time = new Date();
        start_time.setSeconds(start_time.getSeconds() + +process.env.JOIN_TIME/1000 );
        const updateResult = await runRepo.findOneAndUpdate(
          { status: TriviaRunStatus.ACTIVE },
          { $set: { 
            status: TriviaRunStatus.RUNNING,
            start_time: start_time.toString()  
          } 
        });
        result.questions = result.questions;
        result['leaderboard'] = result.leaderboard;
        result['run_id'] = currentRun._id.toString(); 
        result['kickoff'] = +process.env.JOIN_TIME ;  
        result['question_kickoff'] = +process.env.QUESTION_TIME; 
        result['status'] = TriviaRunStatus.RUNNING; 
        if(!updateResult){
          return null;
        }
        //Todo: check insert result
        this.sendMessage(result, "started");
        console.log("Waiting for clients to connect")

        setTimeout(() => {        
          this.triviaWorkflow(result.questions, 0, result['run_id'], questiontimes);
        }, (+process.env.JOIN_TIME)); // first timeout, wait for all clients to connect to socket, bigger timeout than the questions
        
        return result
      }
    }catch (ex) {      
      return null;
    }
  }

  async endTrivia(question_timestamps: Array<string>) {
    try{
      this.disconnectAll();
      let runRepo = await getRepository(Run);
      const updateResult = await runRepo.findOneAndUpdate(
        { status: TriviaRunStatus.RUNNING },
        { $set: { end_time: new Date().getTime().toString(), status: TriviaRunStatus.ENDED, question_times: question_timestamps } }
      );
      return updateResult;
    }catch (ex) {
      return null;
    }
  } 

  async cancelTrivia(question_timestamps: Array<string>) {
    try{
      let runRepo = await getRepository(Run);
      const r = await runRepo.find({
        $or: [
          { status: TriviaRunStatus.RUNNING },
          { status: TriviaRunStatus.ACTIVE }
        ]
      })
      //const run = await runRepo.findOne({ status: TriviaRunStatus.RUNNING }, { status: TriviaRunStatus.ACTIVE })
      
      let updateResult = 0;
      if(r){
        for(const run of r){
          let answerRepo =  await getRepository(Answer);
          const deleteAnswers = await answerRepo.delete({runId: run._id.toString()})
          updateResult = await runRepo.findOneAndUpdate(
            { _id: new ObjectID(run._id) },
            { $set: { end_time: new Date().getTime().toString(), status: TriviaRunStatus.CANCELED, question_times: question_timestamps } }
            ); 
        }   
        
        this.sendMessage(null, "cancel");
        this.disconnectAll();
        return updateResult;
      }
      
      return null;
    }catch (ex) {
      return null;
    }
  } 

  async triviaWorkflow(triviaQuestions: Array<Question>, currentPosition: number, run_id: string, timestamps: Array<string>) {
    try{
      let runRepo = await getRepository(Run);
      const run = await runRepo.findOne({
        $or: [
          { status: TriviaRunStatus.RUNNING },
          { status: TriviaRunStatus.ACTIVE }
        ]
      })


      if(run && run._id.toString() == run_id){
        if(triviaQuestions.length > currentPosition){
          let currentQuestion: any = triviaQuestions[currentPosition];
          currentQuestion = JSON.parse(JSON.stringify(currentQuestion));
          currentQuestion['solution'] = '';
          currentQuestion['question_id'] = currentPosition;   
          this.sendMessage(currentQuestion, "question_start"); 
          setTimeout(() => {            
            this.sendMessage(currentQuestion, "question"); 
            timestamps.push(new Date().getTime().toString());
            currentQuestion = triviaQuestions[currentPosition]; 
            setTimeout(() => {
              this.sendMessage(currentQuestion, "question_end");   
              setTimeout(() => {   
                this.triviaWorkflow(triviaQuestions, (currentPosition + 1), run_id, timestamps)
              }, +process.env.QUESTION_BUFFER);
            }, (+process.env.QUESTION_TIME) - (+process.env.QUESTION_BUFFER))     
          }, (+process.env.QUESTION_BUFFER));
        } else{
          this.endTrivia(timestamps);
          let scores = await this.calculateScores(run_id);
          this.sendMessage(scores, "ended");
          return scores;
        }
      }
      return null; 
    }catch (ex) {
      return null;
    }

  }


  async calculateScores(run_id: string){
    run_id = run_id.toString();
    let answerRepo = await getRepository(Answer);
    let answers = await answerRepo.find({ where: {  'runId': run_id  } });
    let runRepo = await getRepository(Run);
    let currentRun = await runRepo.findOne({ where: { _id:  new ObjectID(run_id) } });

    let triviaRepo = await getRepository(Trivia);
    let trivia = await triviaRepo.findOne( { where: {  'id': currentRun.id.toString()  } }); //TODO: str

    let results: any = {};
    let currentAffiliate = '';   
    let currentQuestion = null;

    for (const answer of answers) {
      currentAffiliate = answer.group;
      currentQuestion = answer.questionId;
      if(answer.answer == trivia.questions[+answer.questionId].solution) {
        let score = 300;
        let max_time: any = new Date(+currentRun.question_times[+answer.questionId]);
        let answer_time: any = new Date(+answer.responseTime);
        let second_diff = (+process.env.QUESTION_TIME  - Math.abs(answer_time - max_time)) / 1000;

        if (second_diff > 0) {
          score = score + (2 * second_diff);
          if(!results[currentAffiliate]){
            results[currentAffiliate] = {};
            results[currentAffiliate][currentQuestion] = score;
          }
          if(!results[currentAffiliate][currentQuestion]){
            results[currentAffiliate][currentQuestion] = score;
          }
          if(results[currentAffiliate][currentQuestion] && +results[currentAffiliate][currentQuestion] < score) {
            results[currentAffiliate][currentQuestion] = score;
          } 
        }
      } else {
        //init affiliate with 0
        if(!results[currentAffiliate]) {
          if(!results[currentAffiliate])
              results[currentAffiliate] = {};
            results[currentAffiliate][currentQuestion] = 0;
        }
      }
    }
    let combinedResults: any = {};
    Object.keys(results).forEach(key => {
      combinedResults[key] = Object.values(results[key]).reduce((a: number, b: number) => a + b, 0)
    })

    const updateResult = await runRepo.findOneAndUpdate(
      { _id:  new ObjectID(run_id) },
      { $set: { results: combinedResults }}
    );  

    return updateResult;
  }

  async totalScores(skipped: string[] = []){
    let runRepo = await getRepository(Run);
    let runs = await runRepo.find({});
    let totalScores:any = {};
    runs.forEach(function (run: any) { 
      if(run.results){
        for (const [key, value] of Object.entries(run.results)) {
          if(skipped && skipped.includes(key))
            continue;
          if(totalScores[key]) {
            totalScores[key] = totalScores[key] + value;
          }else{
            totalScores[key] = value;
          }
        }
      }
    })
   
    let items = Object.keys(totalScores).map(function(key) {
      return [key, totalScores[key]];
    });

    // Sort the array based on the second element
    items.sort(function(first, second) {
      return second[1] - first[1];
    }); 
    return items
  }

  async cleanup() {
    let answerRepo =  await getRepository(Answer);
    const deleteAnswers = await answerRepo.delete({});
    let runRepo = await getRepository(Run);
    const deleteRuns = await runRepo.delete({});
    return deleteRuns;
  }

  sendMessage(m: string, e:string) {
    try{
      const io = getSocket();
      console.log("socket emit", m,"-", e);
      
      io.timeout(120000).emit(e, {message: m}, (err: any, responses: any) => {
        if (err) {
          console.log(`ERROR: ${err}`)
        }
      });
      return true;
    }catch (ex) {
      return null;
    }
  }

  disconnectAll(){
    // try{
    //   const io = getSocket();
    //   io.disconnectSockets();
    //   return true;
    // }catch (ex) {
    //   return null;
    // }
  }
}