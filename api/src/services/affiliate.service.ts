
import { getRepository } from '../datastore';
import { Affiliate } from '../datastore/entities';

export class AffiliateService { 
  async list() {
    try{
      let repo = await getRepository(Affiliate)
      let affiliates = await repo.find({order: { ['name']: 'ASC'}})
      return affiliates
    }catch (ex) {
      return null;
    }
  }
}