export { User } from './user.entity'
export { Trivia, Question } from './trivia.entity'
export { Answer } from './answer.entity'
export { Run } from './run.entity'
export { Affiliate } from './affiliate.entity'

 