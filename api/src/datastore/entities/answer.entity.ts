
import { Entity, ObjectIdColumn, Column, ObjectID } from "typeorm";

@Entity('answer')
export class Answer {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    triviaId: string;

    @Column()
    answer: string;

    @Column()
    questionId: string;

    @Column()
    runId: string;

    @Column()
    playerName: string;

    @Column()
    group: string;

    @Column()
    score: number;

    @Column()
    responseTime: string;
}