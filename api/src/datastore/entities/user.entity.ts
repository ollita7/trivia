
import { Entity, ObjectIdColumn, Column, ObjectID } from "typeorm";

@Entity('user')
export class User {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    email: string;

    @Column()
    name: string;

    @Column()
    status: string;

    @Column()
    role: string;

    @Column()
    picture: string;

    @Column()
    password: string;

    @Column()
    country: string;

    @Column()
    tenant_id: number;
}