
import { Entity, ObjectIdColumn, Column, ObjectID } from "typeorm";

@Entity('run')
export class Run {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    id: string;

    @Column()
    start_time: string;

    @Column()
    end_time: string;

    @Column()
    question_times: Array<string>;

    @Column()
    questions: number;

    @Column()
    results: Array<any>

    @Column()
    status: string

    @Column()
    leaderboard: boolean

}

