
import { Entity, ObjectIdColumn, Column, ObjectID } from "typeorm";

export class Question {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    question: string;

    @Column()
    answer: Array<any>; //or Answer

    @Column()
    solution: string;
 
}
@Entity('trivia')
export class Trivia {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    name: string;

    @Column()
    id: string;

    @Column()
    leaderboard: boolean;

    @Column()
    questions: Array<Question>;
}

