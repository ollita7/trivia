
import { Entity, ObjectIdColumn, Column, ObjectID } from "typeorm";

@Entity('affiliate')
export class Affiliate {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    name: string;   
}