import { MigrationInterface, QueryRunner } from "typeorm"
import { MongoQueryRunner } from "typeorm/driver/mongodb/MongoQueryRunner";

import { Trivia, Question, Answer } from "../entities";

export class AddTrivia1695127895777 implements MigrationInterface {

    public async up(queryRunner: MongoQueryRunner): Promise<void> {
        let trivia = new Trivia();
        let question1 = new Question();
        question1.question = 'Where does the Concurs De Castells festival take place?';
        question1.answer = [{'A': 'Lisbon, Portugal'}, {'B': 'Medellín, Colombia'},{'C': 'Tarragona, Spain'}, {'D': 'Guadalajara, Mexico'}];
        question1.solution = 'C';

        let question2 = new Question();
        question2.question = 'According to a German market research study, what percentage of patients are reported to prefer fixed treatment therapy over continuous therapy?'
        question2.answer = [{'A': '55%'}, {'B': '65%'},{'C': '75%'}, {'D': '85%'}];
        question2.solution = 'D';

        let question3 = new Question();
        question3.question = 'What is the percentage of assets in the AbbVie pipeline are oncology assets?'
        question3.answer = [{'A': '50%'}, {'B': '25%'},{'C': '45%'}, {'D': '35%'}];
        question3.solution = 'C';

        let question4 = new Question();
        question4.question = 'When was Venetoclax first approved in CLL?'
        question4.answer = [{'A': '2017'}, {'B': '2015'},{'C': '2016'}, {'D': 'None of the above'}];
        question4.solution = 'C';

        let question5 = new Question();
        question5.question = 'How is Venetoclax redefining standards of care in CLL?'
        question5.answer = [{'A': 'Based on implementation of Venetoclax-based regimens in international, regional, and local guidelines across lines of therapy'}, 
                            {'B': 'As a combination partner of choice for BTKis seeking to established fixed duration regimens'},
                            {'C': 'As a comparator arm in emerging clinical trials'}, 
                            {'D': 'All of the above'}];
        question5.solution = 'D';
        

        let trivia2 = new Trivia();
        let question12 = new Question();
        question12.question = 'What year was the first AbbVie Global Oncology Forum?'
        question12.answer = [{'A': '2016'}, {'B': '2017'},{'C': '2015'}, {'D': '2018'}];
        question12.solution = 'B';

        let question22 = new Question();
        question22.question = 'Where was the first AbbVie Global Oncology Forum?'
        question22.answer = [{'A': 'Athens Greece'}, {'B': 'Chicago'},{'C': 'Germany'}];
        question22.solution = 'A';

        let question32 = new Question();
        question32.question = 'How many AbbVie Global Oncology Forums have been held?'
        question32.answer = [{'A': '4'}, {'B': '3'},{'C': '5'}];
        question32.solution = 'A';


        let trivia3 = new Trivia();
        let question13 = new Question();
        question13.question = 'Out of all companies specializing in hematology, AbbVie is ranked what number?'
        question13.answer = [{'A': '4'}, {'B': '5'},{'C': '3'}, {'D': '2'}];
        question13.solution = 'C';

        let question23 = new Question();
        question23.question = 'Out of all oncology companies, AbbVie is ranked what number?'
        question23.answer = [{'A': '10'}, {'B': '5'},{'C': '9'}, {'D': '3'}];
        question23.solution = 'C';


        trivia.questions = [question1, question2, question3, question4, question5];         
        trivia2.questions = [question12, question22, question32]; 
        trivia3.questions = [question13, question23]; 
        
        trivia.name = 'LS + CLL';
        trivia2.name = 'MCL + AML';
        trivia3.name = 'MDS + MM + EPCOR';
        trivia.id = '1';
        trivia2.id = '2';
        trivia3.id = '3';

        //Add configuration
        trivia.leaderboard = true;
        trivia2.leaderboard = true;
        trivia3.leaderboard = false;

        let trivias = [trivia, trivia2, trivia3];

        //await queryRunner.insertOne('trivia', trivia);
        await queryRunner.insertMany('trivia', trivias)
    }

    public async down(queryRunner: MongoQueryRunner): Promise<void> {
    }

}
