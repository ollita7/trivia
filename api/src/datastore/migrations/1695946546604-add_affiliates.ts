import { Affiliate } from "../entities";
import { MigrationInterface, QueryRunner } from "typeorm"
import { MongoQueryRunner } from "typeorm/driver/mongodb/MongoQueryRunner";

export class AddAffiliates1695946546604 implements MigrationInterface {

    public async up(queryRunner: MongoQueryRunner): Promise<void> {
        let affiliates: any = [
            "GLOBAL",
            "ADRIATIC",
            "ARGENTINA",
            "AUSTRALIA & NZ",
            "AUSTRIA",
            "BALTIC & UKRAINE",
            "BELGIUM",
            "BRAZIL",
            "BULGARIA",
            "CANADA",
            "CHINA",
            "COLOMBIA",
            "CZECH SLOVAKIA",
            "FINLAND",
            "FRANCE",
            "GERMANY",
            "GREECE",
            "GULF",
            "HUNGARY",
            "IRELAND",
            "ISRAEL",
            "ITALY",
            "JAPAN",
            "LEVANT-E",
            "MEXICO",
            "NETHERLANDS",
            "NORTH WEST AFRICA",
            "POLAND",
            "PORTUGAL",
            "PUERTO RICO",
            "ROMANIA",
            "RUSSIA", 
            "SAUDI ARABIA",            
            "SCANDINAVIA",
            "SOUTH AFRICA",
            "SOUTH KOREA",
            "SOUTHEAST ASIA",
            "SPAIN",
            "SWITZERLAND",
            "TAIWAN",
            "TURKEY",
            "UK"];
            let affiliates_to_add: any = [];
            affiliates.forEach(function (a: any) {
                let new_affiliate = new Affiliate();
                new_affiliate.name = a;
                affiliates_to_add.push(new_affiliate);                
            });
       


        await queryRunner.insertMany('affiliate', affiliates_to_add)

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
