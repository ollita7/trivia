import { IsString, IsNumber } from "kiwi-server";

export class AnswerIn {
  @IsString() triviaId: string;
  @IsString() answer: string;
  @IsString() runId: string;
  @IsString() questionId: string;
  @IsString() playerName: string;
  @IsString() group: string;
  @IsString() responseTime: string;


}


