import { IsString, IsNumber } from "kiwi-server";

export class QuestionIn {
  @IsString() question: string;
  @IsString() answers: Array<any>;
  @IsString() solution: string;
}

export class TriviaIn{  
  @IsString() id: string;
  @IsString() questions: Array<QuestionIn>;
  @IsString() status: string;
}

